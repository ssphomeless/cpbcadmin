'use strict';

/**
 * Module dependencies.
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

//for setting roles:
acl.allow([{
  roles: ['admin'],
  allows: [{
    resources: '/api/members',
    permissions: '*'
  }, {
    resources: '/api/members/:memberId',
    permissions: '*'
  }, {
    resources: '/api/membersImport',
    permissions: ['post']
  }
  ]
}, {
  roles: ['user'],
  allows: [{
    resources: '/api/members',
    permissions: '*'
  }, {
    resources: '/api/members/:memberId',
    permissions: '*'
  },{
    resources: '/api/membersImport',
    permissions: '*'
  }
  ]
}, {
  roles: ['guest'],
  allows: [{
    resources: '/api/members',
    permissions: ['get']
  }, {
    resources: '/api/members/:memberId',
    permissions: ['get']
  }]
}]);


/**
 * Invoke Articles Permissions
 */

/**
 * Check If Articles Policy Allows
 */
exports.isAllowed = function (req, res, next) {

  acl.areAnyRolesAllowed('admin', '/api/membersImport', 'post', function (err, isAllowed) {
    if(isAllowed){
      console.log('is allowed');
    }else {

      console.log('is not allowed');
    }
  });

  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an article is being processed and the current user created it then allow any manipulation
  if (req.member && req.user && req.member.user.id === req.user.id) {
    return next();
  }


  console.log('checking allowed:' + roles + ', path: ' + req.route.path + ', method: ' + req.method.toLowerCase() + '!');
  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {

    if (err) {
      // An authorization error occurred.
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        console.log('access right checking is passed.');
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
