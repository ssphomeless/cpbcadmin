'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Fellowship = mongoose.model('Fellowship'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, fellowship;

/**
 * Fellowship routes tests
 */
describe('Fellowship CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Fellowship
		user.save(function() {
			fellowship = {
				name: 'Fellowship Name'
			};

			done();
		});
	});

	it('should be able to save Fellowship instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fellowship
				agent.post('/fellowships')
					.send(fellowship)
					.expect(200)
					.end(function(fellowshipSaveErr, fellowshipSaveRes) {
						// Handle Fellowship save error
						if (fellowshipSaveErr) done(fellowshipSaveErr);

						// Get a list of Fellowships
						agent.get('/fellowships')
							.end(function(fellowshipsGetErr, fellowshipsGetRes) {
								// Handle Fellowship save error
								if (fellowshipsGetErr) done(fellowshipsGetErr);

								// Get Fellowships list
								var fellowships = fellowshipsGetRes.body;

								// Set assertions
								(fellowships[0].user._id).should.equal(userId);
								(fellowships[0].name).should.match('Fellowship Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Fellowship instance if not logged in', function(done) {
		agent.post('/fellowships')
			.send(fellowship)
			.expect(401)
			.end(function(fellowshipSaveErr, fellowshipSaveRes) {
				// Call the assertion callback
				done(fellowshipSaveErr);
			});
	});

	it('should not be able to save Fellowship instance if no name is provided', function(done) {
		// Invalidate name field
		fellowship.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fellowship
				agent.post('/fellowships')
					.send(fellowship)
					.expect(400)
					.end(function(fellowshipSaveErr, fellowshipSaveRes) {
						// Set message assertion
						(fellowshipSaveRes.body.message).should.match('Please fill Fellowship name');
						
						// Handle Fellowship save error
						done(fellowshipSaveErr);
					});
			});
	});

	it('should be able to update Fellowship instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fellowship
				agent.post('/fellowships')
					.send(fellowship)
					.expect(200)
					.end(function(fellowshipSaveErr, fellowshipSaveRes) {
						// Handle Fellowship save error
						if (fellowshipSaveErr) done(fellowshipSaveErr);

						// Update Fellowship name
						fellowship.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Fellowship
						agent.put('/fellowships/' + fellowshipSaveRes.body._id)
							.send(fellowship)
							.expect(200)
							.end(function(fellowshipUpdateErr, fellowshipUpdateRes) {
								// Handle Fellowship update error
								if (fellowshipUpdateErr) done(fellowshipUpdateErr);

								// Set assertions
								(fellowshipUpdateRes.body._id).should.equal(fellowshipSaveRes.body._id);
								(fellowshipUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Fellowships if not signed in', function(done) {
		// Create new Fellowship model instance
		var fellowshipObj = new Fellowship(fellowship);

		// Save the Fellowship
		fellowshipObj.save(function() {
			// Request Fellowships
			request(app).get('/fellowships')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Fellowship if not signed in', function(done) {
		// Create new Fellowship model instance
		var fellowshipObj = new Fellowship(fellowship);

		// Save the Fellowship
		fellowshipObj.save(function() {
			request(app).get('/fellowships/' + fellowshipObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', fellowship.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Fellowship instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fellowship
				agent.post('/fellowships')
					.send(fellowship)
					.expect(200)
					.end(function(fellowshipSaveErr, fellowshipSaveRes) {
						// Handle Fellowship save error
						if (fellowshipSaveErr) done(fellowshipSaveErr);

						// Delete existing Fellowship
						agent.delete('/fellowships/' + fellowshipSaveRes.body._id)
							.send(fellowship)
							.expect(200)
							.end(function(fellowshipDeleteErr, fellowshipDeleteRes) {
								// Handle Fellowship error error
								if (fellowshipDeleteErr) done(fellowshipDeleteErr);

								// Set assertions
								(fellowshipDeleteRes.body._id).should.equal(fellowshipSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Fellowship instance if not signed in', function(done) {
		// Set Fellowship user 
		fellowship.user = user;

		// Create new Fellowship model instance
		var fellowshipObj = new Fellowship(fellowship);

		// Save the Fellowship
		fellowshipObj.save(function() {
			// Try deleting Fellowship
			request(app).delete('/fellowships/' + fellowshipObj._id)
			.expect(401)
			.end(function(fellowshipDeleteErr, fellowshipDeleteRes) {
				// Set message assertion
				(fellowshipDeleteRes.body.message).should.match('User is not logged in');

				// Handle Fellowship error error
				done(fellowshipDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Fellowship.remove().exec();
		done();
	});
});