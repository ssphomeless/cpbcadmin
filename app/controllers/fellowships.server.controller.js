'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Fellowship = mongoose.model('Fellowship'),
	_ = require('lodash');

/**
 * Create a Fellowship
 */
exports.create = function(req, res) {

	var fellowship = new Fellowship(req.body);
	fellowship.user = req.user;

	fellowship.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(fellowship);
		}
	});
};

/**
 * Show the current Fellowship
 */
exports.read = function(req, res) {
	res.jsonp(req.fellowship);
};

/**
 * Update a Fellowship
 */
exports.update = function(req, res) {
	var fellowship = req.fellowship ;

	fellowship = _.extend(fellowship , req.body);

	fellowship.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(fellowship);
		}
	});
};

/**
 * Delete an Fellowship
 */
exports.delete = function(req, res) {
	var fellowship = req.fellowship ;

	fellowship.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(fellowship);
		}
	});
};

/**
 * List of Fellowships
 */
exports.list = function(req, res) { 
	Fellowship.find().sort('-created').populate('user', 'displayName').exec(function(err, fellowships) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(fellowships);
		}
	});
};

exports.fellowshipByMemberID = function(req, res) {

	var memberId =req.params.memberId;

	Fellowship.find( {participators: memberId}).exec( function(err, fellowships) {
		res.jsonp(fellowships);
	});
};

/**
 * Fellowship middleware
 */
exports.fellowshipByID = function(req, res, next, id) { 
	Fellowship.findById(id)
		.populate('user', 'displayName')
		.populate('participators')
		.exec(function(err, fellowship) {
		if (err) return next(err);
		if (! fellowship) return next(new Error('Failed to load Fellowship ' + id));
		req.fellowship = fellowship ;
		next();
	});
};

/**
 * Fellowship authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.fellowship.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
