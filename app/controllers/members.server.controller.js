'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Fellowship = mongoose.model('Fellowship'),
    errorHandler = require('../../app/controllers/errors.server.controller.js');

//var busboy = require('busboy');
var multer = require('multer');
var config = require('../../app/config/members.server.config.js');
var parseXlsx = require('excel');
var _ = require('lodash');

/**
 * Create a article
 */
exports.create = function (req, res) {
  var member = new Member(req.body);
  member.user = req.user;

  exports.preProcess(member,
      function(successMsg)
      {
        member.save(function (err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {

            res.json(member);
          }
        });
      },
      function(errorMsg)
      {
        return res.status(400).send({
          message: errorMsg
        });
      });

};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  res.json(req.member);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
  var member = req.member;

  member = _.extend(member, req.body);

  exports.preProcess(member,
      function(successMsg)
      {
        member.save(function (err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            res.json(member);
          }
        });
      },
      function(errorMsg)
      {
        return res.status(400).send({
          message: errorMsg
        });
      });


};


/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var member = req.member;

  member.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(member);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {

  Member.find().sort('-created').populate('user', 'displayName').exec(function (err, members) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(members);
    }
  });
};

/**
 * Member middleware
 */
exports.memberByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'members is invalid'
    });
  }

  Member.findById(id).populate('user', 'displayName').exec(function (err, member) {
    if (err) {
      return next(err);
    } else if (!member) {
      return res.status(404).send({
        message: 'No member with that identifier has been found'
      });
    }
    req.member = member;
    next();
  });
};


exports.preProcess = function(member, success, error)
{
  var queryMemberCardID = Member.findOne({_id: {$ne: member._id},
    memberCardID: member.memberCardID
  });

  var queryMemberID = Member.findOne({_id: {$ne: member._id},
    memberID: member.memberID
  });

  queryMemberCardID.exec(function (err, member) {
    if(member) {
      error('Member card id: ' + member.memberCardID + ' exist. Please change it.');
    }
    else {
      queryMemberID.exec(function (err, member) {
        if(member)
        {
          error('Member id: ' + member.memberID + ' exist. Please change it.');
        }
        else {
          success('member checking pass');
        }
      });
    }
  });
};

exports.import = function (req, res) {

  var upload = multer(config.memberExcelUpload).single('file');

  upload(req, res, function (uploadError) {
    if(uploadError) {
      return res.status(400).send({
        message: 'Error occurred while uploading excel file, please contact support'
      });
    } else {

      var fileLocation = config.memberExcelUpload.dest + req.file.filename;
      var targetMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';


      //application/vnd.ms-excel
      if(req.file.mimetype !== targetMimeType)
      {
        return res.status(400).send({
          message: 'Unsupported file type'
        });
      }
      else {
        //do logic
        parseXlsx(fileLocation, function(err, dataList) {
          if(err) {
            return res.status(400).send({
              message: 'Unsupported Excel format. Please follow the standard format.'
            });
          }

          exports.importChecking(dataList, req, res, function(membersList)
          {
            Member.collection.insert(membersList, function (err, docs)
            {
              if (err) {
                console.log('Batch import failure: ' + err);
                return res.status(400).send({
                  message: 'Batch Import failure. Please contact support.'
                });
              } else {
                return res.status(200).send({
                  message: 'Import successful.'
                });
              }
            });

          }, function(errorMsg)
          {
            return res.status(400).send({
              message: errorMsg
            });
          });
        });
      }
    }
  });

};

exports.importChecking =function(list, req, res, success, error) {
  // data is an array of arrays
  var membersList = [];
  var isRepliedError = false;

  _.forEach(list, function (item, index) {

    if (index === 0) {
      //skin first row
      console.log('first row is being skipped');
    }
    else {
      if (item.length > 15) {
        var member = {
          memberID: item[0],
          memberCardID: item[1],
          firstName: item[2],
          lastName: item[3],
          nickName: item[4],
          gender: item[5],
          hkid: item[6],
          birth: item[7],
          address: item[8],
          baptismDate: item[9],
          isBaptism: item[10],
          canVisit: item[11],
          status: item[12],
          remark: item[13],
          phone: item[14],
          email: item[15]
        };
        member.user = req.user._id;

        exports.preProcess(member,
            function (successMsg) {
              membersList.push(member);
              if (membersList.length === list.length - 1) {
                success(membersList);
              }
            },
            function (errorMsg) {

              if(!isRepliedError)
              {
                isRepliedError = true;
                error(errorMsg);
              }
              return false;
            });
      }
      else {
        error('Please make sure the excel contain 15 column.');
      }
    }
  });

};

