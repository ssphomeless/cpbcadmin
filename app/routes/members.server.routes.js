'use strict';

/**
* Module dependencies.
*/


var membersPolicy = require('../../app/policies/members.server.policy.js'),
    members = require('../../app/controllers/members.server.controller.js');



module.exports = function (app) {
  // Members collection routes
  app.route('/api/members').all(membersPolicy.isAllowed)
      .get(members.list)
      .post(members.create);

  // Single member routes
  app.route('/api/members/:memberId').all(membersPolicy.isAllowed)
      .get(members.read)
      .put(members.update)
      .delete(members.delete);

  // Finish by binding the member middleware
  //(Tong comment: for all api rount which has memberId in it, it will call for member function)
  app.param('memberId', members.memberByID);

  app.route('/api/membersImport').all(membersPolicy.isAllowed)
      .post(members.import);

  //app.post('/api/membersImport', upload.single('file'), function (req, res, next) {
  //      // req.file is the `avatar` file
  //      // req.body will hold the text fields, if there were any
  //    console.log('ijo');
  //})


};
