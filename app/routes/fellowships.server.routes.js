'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var fellowships = require('../../app/controllers/fellowships.server.controller');

	// Fellowships Routes
	app.route('/fellowships')
		.get(fellowships.list)
		.post(users.requiresLogin, fellowships.create);

	app.route('/fellowships/:fellowshipId')
		.get(fellowships.read)
		.put(users.requiresLogin, fellowships.hasAuthorization, fellowships.update)
		.delete(users.requiresLogin, fellowships.hasAuthorization, fellowships.delete);

	//get fellowships by member
	app.route('/fellowships/member/:memberId')
		.get(users.requiresLogin, fellowships.fellowshipByMemberID);

	// Finish by binding the Fellowship middleware
	app.param('fellowshipId', fellowships.fellowshipByID);
};
