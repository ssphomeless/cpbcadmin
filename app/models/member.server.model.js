'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Article Schema
 */
var MemberSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  memberID:{
    type: String,
    default: '',
    trim: true
  },
  memberCardID:{
    type: String,
    default: '',
    trim: true
  },
  firstName: {
    type: String,
    default: '',
    trim: true
  },
  lastName: {
    type: String,
    default: '',
    trim: true
  },
  nickName: {
    type: String,
    default: '',
    trim: true
  },
  gender: {
    type: String,
    default: '',
    trim: true
  },
  hkid: {
    type: String,
    default: '',
    trim: true
  },
  birth: {
    type: Date
  },
  phone: {
    type: String,
    default: '',
    trim: true
  },
  email: {
    type: String,
    default: '',
    trim: true
  },
  address: {
    type: String,
    default: '',
    trim: true
  },
  baptismDate: {
    type: Date
  },
  status:{
    type: String,
    default: '1',
    trim: true
  },
  remark:{
    type: String,
    default: '',
    trim: true
  },
  isBaptism:{
    type: Boolean
  },
  canVisit:{
    type: Boolean
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});



mongoose.model('Member', MemberSchema);
