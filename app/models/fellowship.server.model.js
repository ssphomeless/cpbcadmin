'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Fellowship Schema
 */
var FellowshipSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Fellowship name',
		trim: true
	},
	target: {
		type: String,
		default: '',
		trim: true
	},
	participators: [{
		type: Schema.Types.ObjectId,
		ref:'Member'
	}],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Fellowship', FellowshipSchema);
