'use strict';

// Activities controller
angular.module('activities').controller('ActivitiesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Activities', 'Fellowships', 'NgTableParams', '$filter', 'moment',
	function($scope, $stateParams, $location, Authentication, Activities, Fellowships, NgTableParams, $filter, moment) {
		$scope.authentication = Authentication;

		$scope.configConstant = function()
		{
			$scope.activity = {};

			//share for every mode
			$scope.datePopupOpen = function() {
				$scope.datePopup.opened = true;
			};

			$scope.datePopup = {
				opened: false
			};

			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};
		};

		$scope.setup = function()
		{
			$scope.configFellowshipOption();
		};


		$scope.initCreate = function()
		{
			$scope.configConstant();
			$scope.setup();
		};


		$scope.configFellowshipOption = function () {
			//for create/edit
			$scope.queryFellowships = Fellowships.query();
			$scope.queryFellowships.$promise.then(function (result) {
				$scope.activity.fellowships = result;
			});
		};

		// Create new Activity
		$scope.create = function() {
			// Create new Activity object
			var activity = new Activities ({
				name: this.name,
				fellowship: this.activity.fellowship ? this.activity.fellowship._id : '',
				date: this.date
			});

			// Redirect after save
			activity.$save(function(response) {
				$location.path('activities/' + response._id);

				// Clear form fields
				$scope.name = '';
				$scope.activity.fellowship = '';
				$scope.date = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Activity
		$scope.remove = function(activity) {
			if ( activity ) { 
				activity.$remove();

				for (var i in $scope.activities) {
					if ($scope.activities [i] === activity) {
						$scope.activities.splice(i, 1);
					}
				}
			} else {
				$scope.activity.$remove(function() {
					$location.path('activities');
				});
			}
		};

		// Update existing Activity
		$scope.update = function() {
			var activity = $scope.activity;

			activity.$update(function() {
				$location.path('activities/' + activity._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Activities
		$scope.find = function() {
			Activities.query( function(result)
			{
				$scope.activities = result;
				$scope.activityTableParams = new NgTableParams({
					page:1,
					count:10
				}, {
					total:$scope.activities.length ,
					getData: function($defer, params)
					{
						var orderedData = params.filter() ? $filter('filter')($scope.activities, params.filter()) : $scope.activities;
						$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
					}
				});
			});
		};

		// Find existing Activity
		$scope.findOne = function() {
			$scope.configConstant();

			Activities.get({activityId: $stateParams.activityId}, function(result)
			{
				$scope.activity = result;

				if($scope.activity.date)
					$scope.activity.date= moment($scope.activity.date).toDate();

				$scope.setup();
			});
		};

		$scope.editActivityPressed = function(activityID)
		{
			$location.path('activities/' + activityID + '/edit');
		};
	}
]);
