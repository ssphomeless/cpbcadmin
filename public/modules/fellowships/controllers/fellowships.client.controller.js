'use strict';

angular.module('fellowships').constant('_', window._);

// Fellowships controller
angular.module('fellowships').controller('FellowshipsController', ['$scope', '$stateParams', '$location', 'Authentication', 'NgTableParams', '$filter', 'Fellowships', 'Members', '_',
	function($scope, $stateParams, $location, Authentication, NgTableParams, $filter, Fellowships, Members, _) {
		$scope.authentication = Authentication;

		$scope.fellowship = {};

		$scope.initCreate = function ()
		{
			$scope.configParticipatorsOption([]);
		};

		$scope.configParticipatorsOption = function (participatorsList) {
			//for create/edit
			$scope.queryMembers = Members.query();
			$scope.queryMembers.$promise.then(function (result) {
				$scope.fellowship.nonParticipators = result;
				$scope.fellowship.participators = participatorsList;

				var participatorsId = _.pluck($scope.fellowship.participators,'_id');
				$scope.fellowship.nonParticipators = _.reject($scope.fellowship.nonParticipators, function(participator) {
					return _.contains(participatorsId, participator._id);
				});

				$scope.fellowship.participator = {};

				$scope.participatorsTableParams = new NgTableParams({
					page:1,
					count:10
				}, {
					total:$scope.fellowship.participators.length ,
					getData: function($defer, params)
					{
						var orderedData = params.filter() ? $filter('filter')($scope.fellowship.participators, params.filter()) : $scope.fellowship.participators;
						$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
					}
				});

			});
		};

		$scope.addParticipator = function () {

			if($scope.fellowship.participator.selected)
			{
				$scope.fellowship.participators.push($scope.fellowship.participator.selected);
				$scope.participatorsTableParams.reload();

				$scope.fellowship.nonParticipators = _.reject($scope.fellowship.nonParticipators, function(participator) {
					return participator._id === $scope.fellowship.participator.selected._id;
				});
			}
			$scope.fellowship.participator.selected = null;
		};

		$scope.deleteParticipator = function(member)
		{
			$scope.fellowship.nonParticipators.push(member);

			$scope.fellowship.participators = _.reject($scope.fellowship.participators, function(participator) {
				return participator._id === member._id;
			});
			$scope.participatorsTableParams.reload();

		};

		// Create new Fellowship
		$scope.create = function() {

			// Create new Fellowship object
			var fellowship = new Fellowships ({
				name: this.name,
				target: this.target,
				participators: _.pluck(this.fellowship.participators,'_id')
			});

			// Redirect after save
			fellowship.$save(function(response) {
				$location.path('fellowships/' + response._id);

				// Clear form fields
				$scope.name = '';
				$scope.target = '';
				$scope.fellowship = {};
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Fellowship
		$scope.remove = function(fellowship) {
			if ( fellowship ) { 
				fellowship.$remove();

				for (var i in $scope.fellowships) {
					if ($scope.fellowships [i] === fellowship) {
						$scope.fellowships.splice(i, 1);
					}
				}
			} else {
				$scope.fellowship.$remove(function() {
					$location.path('fellowships');
				});
			}
		};

		// Update existing Fellowship
		$scope.update = function() {
			var fellowship = $scope.fellowship;

			fellowship.$update(function() {
				$location.path('fellowships/' + fellowship._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Fellowships
		$scope.find = function() {
			Fellowships.query( function(result)
			{
				$scope.fellowships = result;
				$scope.fellowshipTableParams = new NgTableParams({
					page:1,
					count:10
				}, {
					total:$scope.fellowships.length ,
					getData: function($defer, params)
					{
						var orderedData = params.filter() ? $filter('filter')($scope.fellowships, params.filter()) : $scope.fellowships;
						$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
					}
				});
			});

		};


		$scope.editFellowshipPressed = function(fellowshipId)
		{
			$location.path('fellowships/' + fellowshipId + '/edit');
		};

		// Find existing Fellowship
		$scope.findOne = function() {
			Fellowships.get({fellowshipId: $stateParams.fellowshipId}, function(result)
			{
				$scope.fellowship = result;

				$scope.configParticipatorsOption($scope.fellowship.participators );

			});
		};
	}
]);
