'use strict';

//Setting up route
angular.module('fellowships').config(['$stateProvider',
	function($stateProvider) {
		// Fellowships state routing
		$stateProvider.
		state('listFellowships', {
			url: '/fellowships',
			templateUrl: 'modules/fellowships/views/list-fellowships.client.view.html'
		}).
		state('createFellowship', {
			url: '/fellowships/create',
			templateUrl: 'modules/fellowships/views/create-fellowship.client.view.html'
		}).
		state('viewFellowship', {
			url: '/fellowships/:fellowshipId',
			templateUrl: 'modules/fellowships/views/view-fellowship.client.view.html'
		}).
		state('editFellowship', {
			url: '/fellowships/:fellowshipId/edit',
			templateUrl: 'modules/fellowships/views/edit-fellowship.client.view.html'
		});
	}
]);
