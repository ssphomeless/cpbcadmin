'use strict';

// Configuring the Articles module
angular.module('fellowships').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Fellowships', 'fellowships', 'dropdown', '/fellowships(/create)?');
		Menus.addSubMenuItem('topbar', 'fellowships', 'List Fellowships', 'fellowships');
		Menus.addSubMenuItem('topbar', 'fellowships', 'New Fellowship', 'fellowships/create');
	}
]);
