'use strict';

(function() {
	// Fellowships Controller Spec
	describe('Fellowships Controller Tests', function() {
		// Initialize global variables
		var FellowshipsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Fellowships controller.
			FellowshipsController = $controller('FellowshipsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Fellowship object fetched from XHR', inject(function(Fellowships) {
			// Create sample Fellowship using the Fellowships service
			var sampleFellowship = new Fellowships({
				name: 'New Fellowship'
			});

			// Create a sample Fellowships array that includes the new Fellowship
			var sampleFellowships = [sampleFellowship];

			// Set GET response
			$httpBackend.expectGET('fellowships').respond(sampleFellowships);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.fellowships).toEqualData(sampleFellowships);
		}));

		it('$scope.findOne() should create an array with one Fellowship object fetched from XHR using a fellowshipId URL parameter', inject(function(Fellowships) {
			// Define a sample Fellowship object
			var sampleFellowship = new Fellowships({
				name: 'New Fellowship'
			});

			// Set the URL parameter
			$stateParams.fellowshipId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/fellowships\/([0-9a-fA-F]{24})$/).respond(sampleFellowship);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.fellowship).toEqualData(sampleFellowship);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Fellowships) {
			// Create a sample Fellowship object
			var sampleFellowshipPostData = new Fellowships({
				name: 'New Fellowship'
			});

			// Create a sample Fellowship response
			var sampleFellowshipResponse = new Fellowships({
				_id: '525cf20451979dea2c000001',
				name: 'New Fellowship'
			});

			// Fixture mock form input values
			scope.name = 'New Fellowship';

			// Set POST response
			$httpBackend.expectPOST('fellowships', sampleFellowshipPostData).respond(sampleFellowshipResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Fellowship was created
			expect($location.path()).toBe('/fellowships/' + sampleFellowshipResponse._id);
		}));

		it('$scope.update() should update a valid Fellowship', inject(function(Fellowships) {
			// Define a sample Fellowship put data
			var sampleFellowshipPutData = new Fellowships({
				_id: '525cf20451979dea2c000001',
				name: 'New Fellowship'
			});

			// Mock Fellowship in scope
			scope.fellowship = sampleFellowshipPutData;

			// Set PUT response
			$httpBackend.expectPUT(/fellowships\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/fellowships/' + sampleFellowshipPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid fellowshipId and remove the Fellowship from the scope', inject(function(Fellowships) {
			// Create new Fellowship object
			var sampleFellowship = new Fellowships({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Fellowships array and include the Fellowship
			scope.fellowships = [sampleFellowship];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/fellowships\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleFellowship);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.fellowships.length).toBe(0);
		}));
	});
}());