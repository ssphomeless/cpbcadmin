'use strict';

//Fellowships service used to communicate Fellowships REST endpoints
angular.module('fellowships').factory('Fellowships', ['$resource',
	function($resource) {
		return $resource('fellowships/:fellowshipId', { fellowshipId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			getByMemberId: {
				method: 'GET',
				url: '/fellowships/member/:memberId',
				isArray: true,
				params:{
					memberId:'@memberId'
				}
			}
		});
	}
]);
