'use strict';
angular.module('members').constant('_', window._);

angular.module('members').controller('MembersController', ['$scope', '$stateParams', '$location', 'Authentication', 'NgTableParams', 'Fellowships', 'Members', '$filter', 'MembersAPI', 'moment', '_',
  function ($scope, $stateParams, $location, Authentication, NgTableParams, Fellowships, Members, $filter, MembersAPI,moment, _) {
    $scope.authentication = Authentication;

    $scope.member = {};

    $scope.birthPopupOpen = function() {
      $scope.birthPopup.opened = true;
    };

    $scope.birthPopup = {
      opened: false
    };

    $scope.baptismDatePopupOpen = function() {
      $scope.baptismDatePopup.opened = true;
    };

    $scope.baptismDatePopup = {
      opened: false
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    //definition or default value
    $scope.memberStatuses = [
      {id:'', title:''},
      {id:'1', title:'正常'},
      {id:'2', title:'凍結'},
      {id:'3', title:'已轉會'},
      {id:'4', title:'已安息主懷'},
    ];

    //default value
    $scope.status= $scope.memberStatuses[1].id;

    $scope.genders = [
      {id:'', title:''},
      {id:'M', title:'男'},
      {id:'F', title:'女'},
    ];

    $scope.gender = '';

    $scope.isBaptism = false;
    $scope.canVisit = false;

    $scope.configFellowshipsOption = function (fellowshipList)
    {
      Fellowships.query( function(result)
      {
        $scope.member.nonFellowships = result;
        $scope.member.fellowships = fellowshipList;

        var fellowshipsId = _.pluck($scope.member.fellowships ,'_id');
        $scope.member.nonFellowships = _.reject($scope.member.nonFellowships, function(fellowship) {
          return _.contains(fellowshipsId, fellowship._id);
        });
      });
    };

    $scope.configFellowshipsOption([]);



    // Create new Article
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'memberForm');

        return false;
      }

      // Create new Article object
      var member = new Members({
        memberID: this.memberID,
        memberCardID: this.memberCardID,
        firstName: this.firstName,
        lastName: this.lastName,
        nickName: this.nickName,
        gender: this.gender,
        hkid: this.hkid,
        birth: this.birth,
        phone: this.phone,
        email: this.email,
        address: this.address,
        baptismDate: this.baptismDate,
        status: this.status,
        remark: this.remark,
        isBaptism: this.isBaptism,
        canVisit: this.canVisit
      });

      // Redirect after save
      member.$save(function (response) {
        $location.path('members/' + response._id);

        // Clear form fields
        $scope.memberID = '';
        $scope.memberCardID = '';
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.nickName = '';
        $scope.gender = '';
        $scope.hkid = '';
        $scope.birth = '';
        $scope.phone = '';
        $scope.email = '';
        $scope.address = '';
        $scope.baptismDate = '';
        $scope.status = '';
        $scope.remark = '';
        $scope.isBaptism = '';
        $scope.canVisit = '';

      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (member) {
      if (member) {
        member.$remove();

        for (var i in $scope.members) {
          if ($scope.members[i] === member) {
            $scope.members.splice(i, 1);
          }
        }
      } else {
        $scope.member.$remove(function () {
          $location.path('members');
        });
      }
    };

    // Update existing Article
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'memberForm');

        return false;
      }

      var member = $scope.member;

      member.$update(function () {
        $location.path('members/' + member._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };




    $scope.find = function () {

      $scope.queryMembers = Members.query();
      $scope.queryMembers.$promise.then(function (result) {
        $scope.members = result;

        $scope.memberTableParams = new NgTableParams({
          // initial filter
          page:1,
          count:10,
          //sorting: {
          //  gender: 'desc'     // initial sorting
          //}
        }, {
          total:$scope.members.length ,
          getData: function($defer, params)
          {
            var orderedData = params.filter() ? $filter('filter')($scope.members, params.filter()) : $scope.members;

            //this.firstName = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            //this.lastName = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

            //params.total(orderedData.length); // set total for recalc pagination
            //$defer.resolve(this.firstName, this.lastName);

            //$defer.resolve(orderedData);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
      });
    };

    $scope.findOne = function () {
      $scope.member = Members.get({
        memberId: $stateParams.memberId
      }, function (err, member) {

        Fellowships.getByMemberId({
          memberId: $stateParams.memberId
        }, function (results)
        {
          $scope.member.fellowships = results;

          if($scope.member.birth)
            $scope.member.birth = moment($scope.member.birth).toDate();

          if($scope.member.baptismDate)
            $scope.member.baptismDate = moment($scope.member.baptismDate).toDate();
        });

      });
    };

    $scope.editMemberPressed = function(memberID)
    {
      $location.path('members/' + memberID + '/edit');
    };

    //upload path
    $scope.addFile = function(element) {
      $scope.$apply(function($scope) {

        //for (var i = 0; i < element.files.length; i++) {
        //  alert("Filename: " + element.files[i].name);
        //  alert("Type: " + element.files[i].type);
        //  alert("Size: " + element.files[i].size + " bytes");
        //}

        if(element.files[0].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        {
          alert('Please select excel file (.xlsx)');
          $scope.files = null;
          return;
        }

        $scope.files = element.files;
      });
    };


    $scope.uploadFile = function() {
      MembersAPI.importCSV($scope.files,
          function( msg ) // success
          {
            alert(msg.message);
            $location.path('members/list');
          },
          function( msg ) // error
          {
            $scope.error = msg.message;
          });
    };

  }
]);
