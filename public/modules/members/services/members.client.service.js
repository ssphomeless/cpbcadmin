'use strict';

angular.module('members').factory('Members', ['$resource',
  function ($resource) {
    return $resource('api/members/:memberId', {
      memberId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

angular.module('members').factory('MembersAPI', ['$http',
  function($http) {
    return {
      importCSV : function( files,success, error )
      {
        var fd = new FormData();
        var url = 'api/membersImport';

        angular.forEach(files,function(file){
          fd.append('file',file);
        });

        //sample data
        var data ={
          //name : name,
          //type : type
        };

        fd.append('data', JSON.stringify(data));

        $http.post(url, fd, {
          withCredentials : false,
          headers : {
            'Content-Type' : undefined
          },
          transformRequest : angular.identity
        })
            .success(function(data)
            {
              success(data);
            })
            .error(function(data)
            {
              error(data);
            });
      }
    };

  }

]);




