'use strict';

// Configuring the Articles module
angular.module('members').run(['Menus',
  function (Menus) {

    //this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position, iconClass) {
    //this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {


    Menus.addMenuItem('topbar', 'Members', 'members', 'dropdown', '', false, ['*']);
    Menus.addSubMenuItem('topbar', 'members', 'List Members', 'members/list');
    Menus.addSubMenuItem('topbar', 'members', 'Create Members', 'members/create');
    Menus.addSubMenuItem('topbar', 'members', 'Import Members', 'members/import', '', false, ['admin']);

  }
]);
