'use strict';

// Setting up route
angular.module('members').config(['$stateProvider', '$ocLazyLoadProvider',
  function ($stateProvider, $ocLazyLoadProvider) {

    //$ocLazyLoadProvider.config({
    //  // Set to true if you want to see what and when is dynamically loaded
    //  debug: true
    //});


    $stateProvider
        .state('membersList', {
          url: '/members/list',
          templateUrl: 'modules/members/views/list-members.client.view.html'
        })
        .state('membersCreate', {
          url: '/members/create',
          templateUrl: 'modules/members/views/create-member.client.view.html',
          data: {
            roles: ['admin', 'user']
          }
        })
        .state('membersEdit', {
          url: '/members/:memberId/edit',
          templateUrl: 'modules/members/views/edit-member.client.view.html',
          data: {
            roles: ['admin', 'user']
          }
        })
        .state('membersImport', {
          url: '/members/import',
          templateUrl: 'modules/members/views/import-members.client.view.html',
          data: {
            roles: ['admin']
          }
        })
        .state('membersUpdated', {
            url: '/members/:memberId',
            templateUrl: 'modules/members/views/view-member.client.view.html',
            data: {
                roles: ['admin', 'user']
            }
        });
  }
]);
